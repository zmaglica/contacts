# Contact List

“Contact List” web application using Laravel PHP frameworks and ReactJS. 
## Installation

1. Make sure that you have composer and NPM installed
2. Install Laravel dependencies: `composer install`
3. Install NPM dependencies: `npm install`
4. Run database migration: `php artisan migrate`
5. Enjoy :)

## Built With

* [Laravel](https://laravel.com/) - The web framework used
* [ReactJS](https://reactjs.org/) - The front end framework used
* [NPM](https://www.npmjs.com/) - Dependency Management
