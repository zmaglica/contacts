<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Phone;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class ContactController extends Controller
{
    /**
     * Display all contacts
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        return Contact::all()->load("phones");
    }

    /**
     * Store a newly created contact in database.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'first_name'    => 'required',
                'last_name'     => 'required',
                'email'         => 'required|email',
                'favorite'      => 'boolean',
                'profile_photo' => 'mimes:jpeg,jpg,png|required',
            ]
        );
        $contact = new Contact($request->all());

        if ($request->hasFile('profile_photo')) {
            $contact->profile_photo = $this->imageUpload($request->file('profile_photo'));
        }
        $contact->save();
        $phones = json_decode($request->phones, true);
        $phoneModels = [];
        foreach ($phones as $phone) {
            if ($phone) {
                $phoneModels[] = new Phone($phone);
            }
        }
        $contact->phones()->saveMany($phoneModels);

        return response()->json($contact, 201);
    }

    /**
     * Display contact
     *
     * @param Contact $contact
     * @return Contact
     */
    public function show(Contact $contact)
    {
        return response()->json($contact->load("phones"));

    }

    /**
     * Update contact
     *
     * @param Request $request
     * @param Contact $contact
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Contact $contact)
    {
        $this->validate(
            $request,
            [
                'first_name'    => 'required',
                'last_name'     => 'required',
                'email'         => 'required|email',
                'favorite'      => 'boolean',
                'profile_photo' => 'mimes:jpeg,jpg,png',
            ]
        );
        /**
         * Load data from relationship table
         */
        $contact->load("phones");
        /**
         * Fill data to fillable fields
         */
        $contact->update($request->all());
        /**
         * Check for new file upload. If we have new file uplaod, old pphoto will be deleted and path
         * to new photo will be saved to DB
         */
        if ($request->hasFile('profile_photo')) {
            /**
             * Checking if file existis to avoid errors
             */
            $filePath = public_path('uploads')."/".$contact->profile_photo;
            if (file_exists($filePath)) {
                /**
                 * Delete file
                 */
                unlink(public_path('uploads')."/".$contact->profile_photo);
            }
            /**
             * Save new file
             */
            $contact->profile_photo = $this->imageUpload($request->file('profile_photo'));
        }
        /**
         * If request has phones we will loop over them and add new ones and update old ones
         */
        if ($request->has('phones')) {
            $phones = json_decode($request->phones, true);
            $phoneModels = [];
            $contact->phones()->whereNotIn("id", array_column($phones, "id"))->delete();
            foreach ($phones as $phone) {
                if ($phone) {
                    if (array_key_exists("id", $phone)) {
                        $currentPhone = $contact->phones->find($phone["id"]);
                        $currentPhone->phone = $phone["phone"];
                        $currentPhone->label = $phone["label"];
                        $phoneModels[] = $currentPhone;
                    } else {
                        $phoneModels[] = new Phone($phone);
                    }
                }
            }
            $contact->phones()->saveMany($phoneModels);
        }


        return response()->json($contact, 200);
    }

    /**
     * Change contact favorite
     *
     * @param Contact $contact
     * @return \Illuminate\Http\JsonResponse
     */

    public function favorite(Contact $contact)
    {
        $contact->favorite = !$contact->favorite;

        $contact->save();
        return response()->json($contact, 200);
    }

    /**
     * Delete contact
     *
     * @param Contact $contact
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Contact $contact)
    {
        $filePath = public_path('uploads')."/".$contact->profile_photo;
        if (file_exists($filePath)) {
            unlink(public_path('uploads')."/".$contact->profile_photo);
        }
        $contact->delete();

        return response()->json(null, 204);
    }

    /**
     * @param $file
     * @return string
     */

    private function imageUpload(UploadedFile $file)
    {
        $imageName = str_random(10).'.'.$file->getClientOriginalExtension();
        $file->move(public_path('uploads'), $imageName);

        return $imageName;
    }
}
