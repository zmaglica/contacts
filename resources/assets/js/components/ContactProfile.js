import React from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom'


export default class ContactProfile extends React.Component {

    state = {
        first_name: "",
        last_name: "",
        email: "",
        favorite: "0",
        profile_photo: "",
        phones: [
            {"label": "", "phone": ""},
        ],
        errors: {}
    }

    componentDidMount() {
        if (this.props.location.state !== undefined && "contact" in this.props.location.state) {
            this.setState(this.props.location.state.contact)
        } else if (this.props.match.params.id) {
            axios.get(`${siteUrl}/api/contacts/${this.props.match.params.id}`)
                .then(res => {
                    console.log(res);
                    const contacts = res.data;
                    this.setState(contacts);
                })
                .catch((err) => {
                    toast.error(err.response.data.message);
                })

        }
    }


    render() {
        const {errors, phones, first_name, last_name, email, favorite, profile_photo} = this.state;
        return (
            <div id="profile_view">
                <div className="container">

                    <div className="row">
                        <div className="offset-lg-4 col-lg-4 col-sm-6 col-12 main-section text-center">
                            <div className="row">
                                <div className="col-lg-12 col-sm-12 col-12 profile-header">
                                    <span className="float-right">
                                        <Link to={"/"}>
                                            <i className="fa fa-arrow-left"></i>
                                        </Link>
                                    </span>
                                </div>
                            </div>
                            <div className="row user-detail">
                                <div className="col-lg-12 col-sm-12 col-12">
                                    <img src={`${siteUrl}/uploads/${profile_photo}`}
                                         className="rounded-circle img-thumbnail"/>
                                    <h5>{first_name} {last_name}</h5>
                                    <p><i className="fa fa-envelope" aria-hidden="true"></i> {email}</p>

                                    <hr/>
                                    {phones.map((phone, index) =>
                                        <div className="row" key={index}>
                                            <div className="col-lg-12 col-sm-12 col-12">
                                                <div className="row">
                                                    <div className="col-lg-4 col-sm-4 col-4 text-left">
                                                        <h2>{phone.label} </h2>
                                                    </div>
                                                    <div className="col-lg-8 col-sm-8 col-8 text-right">
                                                        <h2><i className="fa fa-phone"
                                                               aria-hidden="true"></i> {phone.phone}</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )}

                                    <hr/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}