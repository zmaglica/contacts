import React from 'react';
import axios from 'axios';
import {toast} from 'react-toastify';
import {Link} from 'react-router-dom'


export default class ContactForm extends React.Component {
    state = {
        first_name: "",
        last_name: "",
        email: "",
        favorite: "0",
        profile_photo: "",
        phones: [
            {"label": "", "phone": ""},
        ],
        errors: {}
    }

    componentDidMount() {
        if (this.props.location.state !== undefined && "contact" in this.props.location.state) {
            this.setState(this.props.location.state.contact)
        } else if (this.props.match.params.id) {
            axios.get(`${siteUrl}/api/contacts/${this.props.match.params.id}`)
                .then(res => {
                    console.log(res);
                    const contacts = res.data;
                    this.setState(contacts);
                })
                .catch((err) => {
                    toast.error(err.response.data.message);
                })

        }
    }

    handleChange = (e) => {
        let errors = this.state.errors;
        if (e.target.value.length === 0) {

            errors[e.target.name] = "This field is required";

        }
        else if (e.target.name === "email") {
            if (e.target.value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
                delete errors[e.target.name];
            }
            else {
                errors[e.target.name] = "Incorrect email address";
            }
        }
        else {
            delete errors[e.target.name];
        }
        this.setState({
            errors: errors,
            [e.target.name]: e.target.value
        });
    }
    handleAddNewPhone = () => {
        let allPhones = this.state.phones;
        allPhones.push({"label": "", "phone": ""})
        this.setState({
            phones: allPhones
        })
    }
    handleRemovePhone = (index) => {
        let phones = this.state.phones;
        delete(phones[index]);
        this.setState({
            phones: phones
        })
    }
    handlePhoneChange = (e, index, field) => {
        let phones = this.state.phones;
        let errors = this.state.errors;
        if (e.target.value.length === 0) {

            errors[e.target.name] = "This field is required";

        }
        phones[index][field] = e.target.value;
        this.setState({
            errors: errors,
            phones: phones
        })
    }
    handleAjaxErrors = (err) => {
        let ajaxErros = {};
        toast.error(err.response.data.message);
        for (let property in err.response.data.errors) {
            ajaxErros[property] = err.response.data.errors[property].join(" ");
        }
        this.setState({errors: ajaxErros})
    }
    handleResetForm = () => {
        document.getElementById('profile_photo').value = "";

        this.setState({
            first_name: "",
            last_name: "",
            email: "",
            favorite: "0",
            profile_photo: "",
            phones: [
                {"label": "", "phone": ""},
            ],
            errors: {}
        })
    }
    handleContactDelete = (contactId) => {
        if (confirm('Are you sure you want delete this contact?')) {

            axios.delete(`${siteUrl}/api/contacts/${contactId}`)
                .then(res => {
                    toast.success("Contact deleted");
                    this.props.history.push('/');
                })
                .catch((err) => {
                    console.log(err.response);
                    toast.error(err.response.statusText);
                })
        }


    }
    handleSubmitForm = () => {
        let errors = this.state.errors;
        if (this.state.first_name.length === 0) {
            errors["first_name"] = "This field is required";
        }
        if (this.state.last_name.length === 0) {
            errors["last_name"] = "This field is required";
        }
        if (this.state.email.length === 0) {
            errors["email"] = "This field is required";
        }
        if (this.state.profile_photo.length === 0) {
            errors["profile_photo"] = "This field is required";
        }
        if (Object.keys(errors).length > 0 && errors.constructor === Object) {
            this.setState({errors: errors})
        }
        else {
            const imagefile = document.getElementById('profile_photo');

            let formData = new FormData();
            formData.append("first_name", this.state.first_name);
            formData.append("last_name", this.state.last_name);
            formData.append("email", this.state.email);
            formData.append("favorite", this.state.favorite);
            if (imagefile.files.length > 0) {
                formData.append("profile_photo", imagefile.files[0]);
            }
            formData.append("phones", JSON.stringify(this.state.phones));
            let axiosConfig = {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            };

            let url = `${siteUrl}/api/contacts`;
            let successMessage = "New contact added successfully!";
            if (this.props.match.params.id) {
                url = `${siteUrl}/api/contacts/${this.props.match.params.id}`;
                successMessage = "Contact added successfully!"
            }

            axios.post(url, formData, axiosConfig)
                .then((data) => {
                    toast.success(successMessage);
                    console.log(this.props);
                    this.props.history.push('/');

                })
                .catch((err) => {
                    this.handleAjaxErrors(err)
                })
        }

    }

    render() {
        const {errors, phones, first_name, last_name, email, favorite} = this.state;
        const listPhoneNumbers = phones.map((number, index) =>
            <div key={index} className="input-group">
                <div className="col-md-11">
                    <div className="col-half">
                        <input type="text" placeholder="Phone" name={`phones[${index}].phone`} value={number.phone}
                               onChange={(e) => this.handlePhoneChange(e, index, "phone")}/>
                    </div>
                    <div className="col-half">
                        <input type="text" placeholder="Label" name={`phones[${index}].label`} value={number.label}
                               onChange={(e) => this.handlePhoneChange(e, index, "label")}/>
                    </div>
                </div>
                {index > 0 &&
                <div className="col-md-1">
                    <span onClick={() => this.handleRemovePhone(index)}><i className="fa fa-trash fa-3x"></i></span>
                </div>
                }
            </div>
        );

        return (
            <div className="form-container">
                <div className="row">
                    <div className="col-md-12">
                        <h4 className="float-left">Contact info</h4>
                        <span className="float-right">
                    <Link
                        to={"/"}
                    >
                        <i className="fa fa-arrow-left"></i>
                    </Link>
                        </span>
                    </div>
                    <div className="input-group input-group-icon">
                        <input type="text" name="first_name"
                               value={first_name}
                               className={`form-control${"first_name" in errors ? " is-invalid" : ""}`}
                               placeholder="First Name" onChange={(e) => this.handleChange(e)}/>
                        <div className="input-icon"><i className="fa fa-user"></i></div>
                        <div className="invalid-feedback">{"first_name" in errors ? errors["first_name"] : ""}</div>
                    </div>
                    <div className="input-group input-group-icon">
                        <input type="text" name="last_name" placeholder="Last Name"
                               value={last_name}
                               className={`form-control${"last_name" in errors ? " is-invalid" : ""}`}
                               onChange={(e) => this.handleChange(e)}/>
                        <div className="input-icon"><i className="fa fa-user"></i></div>
                        <div className="invalid-feedback">{"last_name" in errors ? errors["last_name"] : ""}</div>
                    </div>
                    <div className="input-group input-group-icon">
                        <input type="email" name="email" placeholder="Email Adress"
                               value={email}
                               onChange={(e) => this.handleChange(e)}
                               className={`form-control${"email" in errors ? " is-invalid" : ""}`}/>
                        <div className="input-icon"><i className="fa fa-envelope"></i></div>
                        <div className="invalid-feedback">{"email" in errors ? errors["email"] : ""}</div>
                    </div>
                    <div className="input-group input-group-icon">
                        <select name="favorite" value={parseInt(favorite)} onChange={(e) => this.handleChange(e)}>
                            <option value={0}>No</option>
                            <option value={1}>Yes</option>
                        </select>
                        <div className="input-icon"><i className="fa fa-star"></i></div>
                    </div>
                    <div className="input-group input-group-icon">
                        <input type="file" id="profile_photo" name="profile_photo" placeholder="Profile picture"
                               accept="image/*" onChange={(e) => this.handleChange(e)}
                               className={`form-control${"profile_photo" in errors ? " is-invalid" : ""}`}/>
                        <div className="input-icon"><i className="fa fa-image"></i></div>
                        <div
                            className="invalid-feedback">{"profile_photo" in errors ? errors["profile_photo"] : ""}</div>
                    </div>
                    <div className="row">
                    <div className="col-md-12">
                        <h4>Phones</h4>
                    </div>
                    </div>
                    <div className="row">
                        <div>
                            {listPhoneNumbers}
                        </div>
                        <div className="col-md-12">
                            <button type="button" className="btn btn-primary float-right"
                                    onClick={this.handleAddNewPhone}>Add new phone
                            </button>
                        </div>
                    </div>
                    <div className="col-md-12 mt-4">
                        <div className="float-right">
                            <button type="button" className="btn btn-secondary" onClick={this.handleSubmitForm}>Submit
                            </button>
                        </div>
                        <div className="float-left">
                            <button type="button" className="btn btn-danger"
                                    onClick={this.props.match.params.id ? () => this.handleContactDelete(this.props.match.params.id) : this.handleResetForm}>{this.props.match.params.id ? "Delete" : "Cancel"}</button>
                        </div>
                    </div>

                </div>
            </div>
        )

    }
}
