import React from 'react';

import {Link} from 'react-router-dom'

class ContactForm extends React.Component {
    render() {
        const {contact} = this.props;
        return <div className="col-lg-4 col-sm-4 col-12">
            <div className="row-fluid section">

                <div className="col-lg-12 col-sm-12 col-12">
                    <div className="float-left">
                    <span onClick={(e) => this.props.handleFavoriteChange(contact.id)}>
                        <i className={`${contact.favorite === 1 ? "fa fa-heart" : "fa fa-heart-o"} fa-red-color`}></i>
                    </span>
                    </div>
                    <div className="float-right">
                        <Link
                            to={{
                                pathname:`/edit/${contact.id}`,
                                state: {contact: contact}
                            }}
                        >
                            <i className="fa fa-edit"></i>
                        </Link>
                        <span onClick={(e) => this.props.handleContactDelete(contact.id)}>
                        <i className="fa fa-remove"></i>
                    </span>
                    </div>
                    <Link
                        to={{
                            pathname: `/show/${contact.id}`,
                            state: {contact: contact}
                        }}
                    >
                        <img src={`${siteUrl}/uploads/${contact.profile_photo}`}
                             className="rounded-circle img-thumbnail"/>
                        <h1>{contact.first_name} {contact.last_name}</h1>
                    </Link>
                </div>
            </div>
        </div>
    }
}

export default ContactForm;