import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Homepage from './Homepage'
import ContactForm from './ContactForm'
import ContactProfile from './ContactProfile'

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Main = () => (
    <main>
        <ToastContainer autoClose={8000} />
        <Switch>
            <Route exact path='/' component={Homepage}/>
            <Route path='/new' component={ContactForm}/>
            <Route path='/edit/:id' component={ContactForm}/>
            <Route path='/show/:id' component={ContactProfile}/>

        </Switch>
    </main>
)

export default Main