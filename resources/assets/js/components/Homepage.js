// Homepage.js

import React from 'react';
import axios from 'axios';
import {toast} from 'react-toastify';
import {Link} from 'react-router-dom'



import ContactCard from './ContactCard'

class Homepage extends React.Component {

    state = {
        contacts: [],
        searchString: "",
        showOnlyFavorites: false
    }

    handleSearchChange = (e) => {
        this.setState({
            searchString: e.target.value
        })
    }

    handleShowOnlyFavorites = () => {
        this.setState({
            showOnlyFavorites: !this.state.showOnlyFavorites
        })
    }

    handleContactDelete = (contactId) => {
        if (confirm('Are you sure you want delete this contact?')) {

            axios.delete(`${siteUrl}/api/contacts/${contactId}`)
                .then(res => {
                    let contacts = this.state.contacts.filter(item => item.id !== contactId)
                    this.setState({
                        contacts: contacts
                    })

                    toast.success("Contact deleted");
                })
                .catch((err) => {
                    console.log(err.response);
                    toast.error(err.response.statusText);
                })
        }


    }

    handleFavoriteChange = (contactId) => {


        axios.post(`${siteUrl}/api/favorites/${contactId}`)
            .then(res => {
                let contacts = this.state.contacts;


                const foundIndex = contacts.findIndex(x => x.id == contactId);
                console.log(contacts[foundIndex]);

                contacts[foundIndex].favorite = !contacts[foundIndex].favorite ? 1 : 0;
                this.setState({
                    contacts:contacts
                })
                toast.success("Contact successfully edited");
            })
            .catch((err) => {
                console.log(err.response);
                toast.error(err.response.statusText);
            })
    }


    componentDidMount() {

        axios.get(`${siteUrl}/api/contacts`)
            .then(res => {
                console.log(res);
                const contacts = res.data;
                this.setState({contacts});
            })
            .catch((err) => {
                toast.error(err.response.data.message);
            })

    }

    render() {
        let contacts = this.state.contacts.filter(contact => (contact.first_name + ' ' + contact.last_name).toLowerCase().includes(this.state.searchString.toLowerCase()));
        if (this.state.showOnlyFavorites) {
            contacts = contacts.filter(contact => (contact.favorite === 1))
        }
        return (
            <div>
                <div className="container mt-5">
                    <div className="row">
                        <div className="col-lg-9 col-sm-9 col-9">
                            <div className="input-group input-group-icon">
                                <input type="text" name="search"
                                       placeholder="Search for contacts..."
                                       onChange={(e) => this.handleSearchChange(e)}/>
                                <div className="input-icon"><i className="fa fa-search"></i></div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-sm-3 col-3">
                            <Link to={"new"}><button className="btn btn-primary addNewButton">Add new</button></Link>
                        </div>

                    </div>
                    <div className="row">
                        <span>Show favorites</span>
                        <label className="switch">
                            <input onChange={() => this.handleShowOnlyFavorites()} type="checkbox"/>
                            <span className="slider round"></span>
                        </label>
                    </div>
                    <div className="row">
                        <div className="col-lg-12 col-sm-12 col-12 main-section">
                            <div className="row text-center">
                                {contacts.map((contact, index) =>
                                    <ContactCard
                                        key={index}
                                        contact={contact}
                                        handleContactDelete={this.handleContactDelete}
                                        handleFavoriteChange={this.handleFavoriteChange}
                                    />
                                )}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Homepage;